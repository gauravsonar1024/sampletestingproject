
import { Component, OnInit } from '@angular/core';
import { RouterModule } from '@angular/router'
import { UtilityService } from '../utility.service';


@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
})

export class Summary implements OnInit {

  credit: any = 0;
  debit: any = 0;
  total: any = 0;
  customers: Array<any>;
  creditCutomerNumber: number = 0;
  debitCustomerNumber: number = 0;
  constructor(private _utilityServices: UtilityService) {

  }
  ngOnInit() {
    this.getCustomers();
  }
  
  getCustomers(){
    this._utilityServices.getCustomers().subscribe(responseObj => {
      this.customers = responseObj;
      for (let cust of this.customers) {
        if (cust.credit > 0) {
          this.credit  += cust.credit;
          if (cust.balance < 0) {
            this.creditCutomerNumber++;
          }
        }else if (cust.debit > 0) {
          this.debit += cust.debit;
          if (cust.balance > 0) {
            this.debitCustomerNumber++;
          }
        }
      }
      this.total=  this.debit - this.credit;
    },
      errorResponse => {
        console.log(errorResponse)
      })



  }
}