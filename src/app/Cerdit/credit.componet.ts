
import { Component, OnInit } from '@angular/core';
import { RouterModule, Router } from '@angular/router'
import { UtilityService } from '../utility.service';


@Component({
  selector: 'app-credit',
  templateUrl: './credit.component.html',
})

export class Credit implements OnInit {

  creditCustomer: Array<any> = [];
  customers: Array<any> = [];

  constructor(private _utilityServices: UtilityService,
    private _rotuer: Router) {
  }
  ngOnInit() {
    this.getCreditCustomers();
  }

  getCreditCustomers() {
    this._utilityServices.getCustomers().subscribe(responseObj => {
      this.customers = responseObj;
      for (let cust of this.customers) {
        if (cust.credit > 0) {
          this.creditCustomer.push(cust);
        }
      }
    },
      errorResponse => {
        console.log(errorResponse)
      })
  }
  navigateToTransaction(custObj) {
    this._utilityServices.setCustObj(custObj);
    this._rotuer.navigate(['transaction']);
  }
}