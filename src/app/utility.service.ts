import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { LOCAL_STORAGE, WebStorageService } from 'angular-webstorage-service';
import { Http, Response, Headers, Request, RequestOptions } from '@angular/http'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Customer } from './customer';


@Injectable()
export class UtilityService {

    public custObj: any;
    private baseUrl: string = 'http://localhost:8080/khata';
    private headers = new HttpHeaders({ 'Content-Type': 'application/json' })
    constructor(@Inject(LOCAL_STORAGE) private storage: WebStorageService, 
    private _http: HttpClient) {

    }

    customers:Customer;
    /* setCustomer(cust: Array<any>) {

        //this.storage.remove('customer')
        if (this.storage.get('customer') != null) {
            this.customers = this.storage.get('customer');
        }
        this.customers.push(cust)
        this.storage.set('customer', this.customers);
        console.log(this.storage.get('customer'));
    } */
    getCustomers(): Observable<any> {
       return this._http.get(this.baseUrl + '/customers', {headers:this.headers});
    }
    createCustomer(customer): Observable<any> {
        return this._http.post(this.baseUrl +'/customer',customer, {headers:this.headers});
    }
    setCustObj(obj) {
        this.custObj = obj;
    }
    getCustObj(): Array<any> {
        return this.custObj
    }

}