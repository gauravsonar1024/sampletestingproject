export class Customer {
    id:number;
    name:string;
    address:string;
    mobile_no:number;
    date:Date;
    comment:string;
    credit:number;
    debit:number;
    balance:number;
}
