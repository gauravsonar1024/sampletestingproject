
import { TransactionComponent } from './transaction/transaction.component'
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router'
import { AppComponent } from './app.component';
import { Summary } from './Summary/summary.componet';
import { Credit } from './Cerdit/credit.componet';
import { Debit } from './Debit/debit.componet';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { AddCustomer } from './Add Customer/addcustomer.componet';
import { ReactiveFormsModule } from '@angular/forms'
import { UtilityService } from './utility.service';
import { StorageServiceModule} from 'angular-webstorage-service';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,Summary,Credit,Debit,AddCustomer, TransactionComponent
  ],
  imports: [
    BrowserModule,RouterModule.forRoot([
     { path :'summary' ,component : Summary},  
     { path :'credit' ,component : Credit},  
     { path :'debit' ,component : Debit},
     { path :'add-customer' ,component : AddCustomer},
     { path :'transaction' ,component : TransactionComponent},
     { path : '', redirectTo : 'summary', pathMatch : 'full' } 
    ]),
    AngularFontAwesomeModule,
    ReactiveFormsModule,
    StorageServiceModule,
    HttpClientModule,
    HttpModule,
    
  ],
  providers: [UtilityService],
  bootstrap: [AppComponent],
  
})
export class AppModule { }
