
import { Component, OnInit } from '@angular/core';
import { RouterModule, Router } from '@angular/router'
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms'
import { UtilityService } from '../utility.service';
import { Customer } from '../customer';

@Component({
  selector: 'app-addcustomer',
  templateUrl: './addcustomer.component.html',
})

export class AddCustomer implements OnInit {

  addCustomerForm: FormGroup;
  customer = new Customer;

  constructor(private fb: FormBuilder,
    private _utilityServices: UtilityService,
    private _rotuer: Router) {

  }

  ngOnInit() {

    this.addCustomerForm = this.fb.group({
      name: ['', Validators.required],
      address: ['', Validators.required],
      date: '',
      comment: '',
      mobile_no: '',
      transactionType: '',
      amount: ''
    });

  }

  addCustomer() {
    this.customer.name = this.addCustomerForm.get('name').value;
    this.customer.address = this.addCustomerForm.get('address').value;
    this.customer.mobile_no = this.addCustomerForm.get('mobile_no').value;
    this.addCustomerForm.get('transactionType').value === 'credit' ? this.customer.credit = this.addCustomerForm.get('amount').value : this.customer.credit = 0;
    this.addCustomerForm.get('transactionType').value === 'debit' ? this.customer.debit = this.addCustomerForm.get('amount').value : this.customer.debit = 0;
    this.customer.comment = this.addCustomerForm.get('comment').value;
    this.customer.date = this.addCustomerForm.get('date').value;
    this.customer.balance = this.customer.debit - this.customer.credit;
    this._utilityServices.createCustomer(this.customer).subscribe(res => {
      console.log(res);
    },err=>{
      console.log(err);
    });
    this._rotuer.navigate(['transaction']);
    this._utilityServices.setCustObj(this.customer);
  }

}

