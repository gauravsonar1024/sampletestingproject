import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms'
import { AddCustomer } from './addcustomer.componet';

@NgModule({
    declarations: [
      AddCustomer
    ],
    imports: [
      BrowserModule,ReactiveFormsModule
    ],
    providers: [],
    bootstrap: [],
    
  })
  export class AppModule { }
  