import { Component, OnInit } from '@angular/core';
import { UtilityService } from '../utility.service';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms'
declare var $ : any;


@Component({
  selector: 'app-transaction',
  templateUrl: 'transaction.component.html',
  
})

export class TransactionComponent implements OnInit {
  
  customer:Array<any>;  
  editForm:FormGroup;
  closeResult: string;
  constructor(private _utilityServices : UtilityService,
  private fb: FormBuilder,){}

  ngOnInit() {
   this.customer = this._utilityServices.getCustObj()
   this.editForm = this.fb.group({
    name: ['', Validators.required],
    address: ['', Validators.required],
    mobile_no: '',
   })
  }
  
  prepareEdit(){
    $('#editModal').modal('show')
  }
  closeModal(id){
   // $(id).modal('hide');
  }
  saveCustomer(){

  }
}
