
import { Component, OnInit } from '@angular/core';
import { UtilityService } from '../utility.service';
import { RouterModule, Router } from '@angular/router'

@Component({
  selector: 'app-debit',
  templateUrl: './debit.component.html',
})

export class Debit implements OnInit {

  debitCustomer: Array<any> = [];
  customers: Array<any> = [];

  constructor(private _utilityServices: UtilityService,
  private _router : Router) {
  }
  ngOnInit() {
    this.getCreditCustomers();
    console.log(this.debitCustomer)
  }

  getCreditCustomers() {
    this._utilityServices.getCustomers().subscribe(responseObj => {
      this.customers = responseObj;
      for (let cust of this.customers) {
        if (cust.debit > 0) {
          this.debitCustomer.push(cust);
        }
      }
    },
      errorResponse => {
        console.log(errorResponse)
      })
  }
  navigateToTransaction(custObj) {
    this._utilityServices.setCustObj(custObj);
    this._router.navigate(['transaction']);
  }
}
