import { AppPage } from './app.po';
import { browser, by, element } from 'protractor';

describe('pass-book-app App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display Summary Page', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Summary');
  });

  it('should display credit button',() =>{
    page.navigateTo();
    expect(page.getCreditButton().getText()).toEqual('Credit')
  });

  it('should route to credit',() =>{
    page.navigateTo();
    page.getCreditButton().click()
    expect(page.getCreditText()).toEqual('Credit')
  });

  it('should display AddCustomer button',() =>{
    page.navigateTo();
    expect(page.getAddCustomerButton().getText()).toEqual('')
  });

  it('should route to AddCustomer',() =>{
    page.navigateTo();
    page.getAddCustomerButton().click()
    expect(page.getAddCustText()).toEqual('Add Customer')
  });
  
  it('should be able to fill form',() =>{
    page.navigateTo();
    page.getAddCustomerButton().click()
    page.getAddCustomerNameField().sendKeys('khan');
    page.getAddCustomerAddeField().sendKeys('Mumbai');
    page.getAddCustomerDateField().sendKeys('21/05/2018');
    page.getAddCustomerDebitField().click();
    page.getAddCustomerAmountField().sendKeys('5000');
    page.getAddCustomerCommentField().sendKeys('Birthday Party')
    browser.pause();
    page.getAddButton().click();
    //expect(page.getAddCustomerNameField().getText())
  });

});
