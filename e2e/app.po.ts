import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('app-summary h2')).getText();
  }
  getCreditText() {
    return element(by.css('app-credit')).getText();
  }

  getAddCustText() {

    return element(by.css('app-addcustomer div div')).getText();
  }

  getCreditButton(){
    return element(by.css('[ng-reflect-router-link="/credit"]'))
  }
  getAddCustomerButton(){
    return element(by.css('[ng-reflect-router-link="/add-customer"]'))
  }
  getAddCustomerNameField(){
    return element(by.css('[formcontrolname="name"]'))
  }
  getAddCustomerAddeField(){
    return element(by.css('[formcontrolname="address"]'))
  }
  getAddCustomerDateField(){
    return element(by.css('[formcontrolname="date"]'))
  }
  getAddCustomerCreditField(){
    return element(by.css('[formcontrolname="credit"]'))
  }
  getAddCustomerDebitField(){
    return element(by.css('[formcontrolname="transactionType"]'))
  }
  getAddCustomerCommentField(){
    return element(by.css('[formcontrolname="comment"]'))
  }
  getAddCustomerAmountField(){
    return element(by.css('[formcontrolname="amount"]'))
  }

  getAddButton(){
    return element(by.css('app-addcustomer button'))
  }
}
